require "uri"
require "find"
require "fileutils"

module Jekyll
module ImageCopier

    class PostFile < StaticFile
        # Initialize a new PostFile.
        #
        # site - The Site.
        # base - The String path to the <source> - /srv/jekyll
        # dir  - The String path between <source> and the file - _posts/somedir
        # name - The String filename of the file - cool.svg
        # dest - The String path to the containing folder of the document which is output - /dist/blog/[:tag/]*:year/:month/:day
        def initialize(site, base, dir, name, dest)
          super(site, base, dir, name)
          @name = name
          @dest = dest
        end
  
        # Obtain destination path.
        #
        # dest - The String path to the destination dir.
        #
        # Returns destination file path.
        def destination(_dest)
          File.join(@dest, @name)
        end
    end

    class Generator < Jekyll::Generator
        def generate(site)
            dest = site.source + "/" + site.config["destination"]

            for collection in site.collections
                collection_name = collection[1].label
                collection_objects = collection[1].docs

                # next unless collection_name == "test"
                collection_permalink = site.config["collections"][collection_name]["permalink"]

                for object in collection_objects
                    uri_elements = URI(object.path).path.split('/')
                    object_name = uri_elements.pop
                    object_dir = uri_elements.join('/').sub! site.source+"/",""
                    
                    # object.content
                    # site.config["url"]

                    images = []
                    Find.find(object_dir) do |path|
                        if path =~ /.*\.png$/ or path =~ /.*\.jpeg$/ or path =~ /.*\.jpg$/ or path =~ /.*\.pdf$/
                            image_uri_elements = URI(path).path.split("/")
                            images << image_uri_elements.last
                        end
                    end
                    dest_dir = (dest + object.url.sub("index", ""))
                    for image in images 
                        site.static_files.push(PostFile.new(site, site.source, object_dir, image, dest_dir))
                    end
                end
            end
        end
    end

  end
  end